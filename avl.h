#include <string>

using namespace std;

/**********************************************************************
 *
 *	You can add things to this file, if necessary.
 *
 * ********************************************************************/

struct student
{
	int id;		 //student ID
	string name; //student name
	float gpa;	 //student GPA

	student()
	{
		id = -1;
		name = "Not Initialized";
		gpa = -1;
	}

	student(int i, string n, float g)
	{
		id = i;
		name = n;
		gpa = g;
	}
};

struct node
{
	student s;		   //student record
	node *left_child;  //a pointer to the left child
	node *right_child; //a poitner to the right child
	int height;
	int bf;
	node(student stud, node *left, node *right)
	{
		s = stud;
		left_child = left;
		right_child = right;
	}
};

class AVL
{
private:
	node *root; //the root node of the tree
public:
	AVL();

	//insert a student record into the tree
	void insert(student s);

	//delete a student record from the tree based on ID
	void deleteID(int id);

	//print the height of the current AVL tree
	void showHeight();

	//print all records, sorted by student ID. This is essentially an in-order traversal of the tree.
	void printAll();

	//return a student's GPA based on his/her ID
	float GPA(int id);

	//return the max GPA of all the students
	float maxGPA();
};
