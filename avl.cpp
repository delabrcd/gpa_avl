#include "avl.h"
#include <iostream>

AVL::AVL()
{
    root = NULL;
}

int height(node *r)
{
    if (r == NULL)
        return -1;
    else
        return r->height;
}

int getBF(node *r)
{
    if (r == NULL)
        return 0;
    return height(r->left_child) - height(r->right_child);
}

int computeHeight(node *r)
{
    return max(height(r->left_child), height(r->right_child)) + 1;
}

node *leftRotate(node *x)
{
    node *y = x->right_child;
    node *beta = y->left_child;

    y->left_child = x;
    x->right_child = beta;

    x->bf = getBF(x);
    x->height = computeHeight(x);
    y->bf = getBF(y);
    y->height = computeHeight(y);

    return y;
}

node *rightRotate(node *y)
{
    node *x = y->left_child;
    node *beta = x->right_child;
    x->right_child = y;
    y->left_child = beta;

    y->bf = getBF(y);
    y->height = computeHeight(y);
    x->bf = getBF(x);
    x->height - computeHeight(x);

    return x;
}

node *balance(node *r)
{
    r->height = computeHeight(r);
    r->bf = getBF(r);
    if (r->bf == 2 && r->left_child->bf >= 0)
    {
        //leftleft case
        return rightRotate(r);
    }
    else if (r->bf == 2 && r->left_child->bf == -1)
    {
        //leftright case
        r->left_child = leftRotate(r->left_child);
        return rightRotate(r);
    }
    if (r->bf == -2 && r->right_child->bf <= 0)
    {
        //rightright case
        return leftRotate(r);
    }
    else if (r->bf == 2 && r->right_child->bf == 1)
    {
        //leftright case
        r->right_child = rightRotate(r->right_child);
        return leftRotate(r);
    }

    return r;
}

node *insert_helper(node *r, student s)
{
    if (r == NULL)
    {
        return new node(s, NULL, NULL);
    }
    if (s.id == r->s.id)
    {
        return r;
    }
    else if (s.id > r->s.id)
    {
        r->right_child = insert_helper(r->right_child, s);
    }
    else
    {
        r->left_child = insert_helper(r->left_child, s);
    }
    return balance(r);
}

//insert a student record into the tree
void AVL::insert(student s)
{
    root = insert_helper(root, s);
}

//Find the min node in the subtree rooted by r.
int findMin(node *r)
{
    if (r->left_child == NULL)
        return r->s.id;
    else
        return findMin(r->left_child);
}

node *remove_helper(node *r, int id)
{
    if (r == NULL)
        return NULL;
    else if (id > r->s.id)
    {
        r->right_child = remove_helper(r->right_child, id);

        return balance(r);
    }
    else if (id < r->s.id)
    {
        r->left_child = remove_helper(r->left_child, id);

        return balance(r);
    }
    else
    {
        if (r->left_child == NULL && r->right_child == NULL)
        {
            //r is a leaf node.
            delete r;
            return NULL;
        }
        else if (r->left_child == NULL)
        {
            node *n = r->right_child;
            delete r;
            return n;
        }
        else if (r->right_child == NULL)
        {
            node *n = r->left_child;
            delete r;
            return n;
        }
        else
        {
            r->s.id = findMin(r->right_child);
            r->right_child = remove_helper(r->right_child, r->s.id);

            return balance(r);
        }
    }
}

//delete a student record from the tree based on ID
void AVL::deleteID(int id)
{
    root = remove_helper(root, id);
    root->height = height(root);
}

//print the height of the current AVL tree
void AVL::showHeight()
{
    std::cout << root->height << std::endl;
}

string prettyStudent(student s)
{
    return ("ID: " + std::to_string(s.id) + "\nName: " + s.name + "\nGPA: " + std::to_string(s.gpa) + "\n\n");
}

void inorder_helper(node *r)
{
    if (r == NULL)
        return;

    inorder_helper(r->left_child);
    cout << prettyStudent(r->s);
    inorder_helper(r->right_child);
}

//print all records, sorted by student ID. This is essentially an in-order traversal of the tree.
void AVL::printAll()
{
    inorder_helper(root);
}

node* gpa_helper(node *r, int id)
{
    if (r == NULL || r->s.id == id)
    {
        return r;
    }

    if (r->s.id < id)
        return gpa_helper(r->right_child, id);

    gpa_helper(r->left_child, id);
}

//return a student's GPA based on his/her ID
float AVL::GPA(int id)
{
    node* r = gpa_helper(root, id);
    return r->s.gpa;
}

float max_gpa_helper(node *r, float current_max)
{
    if (r == NULL)
    {
        return current_max;
    }
    float max = std::max(current_max, r->s.gpa);
    max_gpa_helper(r->left_child, max);
    max_gpa_helper(r->right_child, max);
}

//return the max GPA of all the students
float AVL::maxGPA()
{
    return max_gpa_helper(root, 0);
}
